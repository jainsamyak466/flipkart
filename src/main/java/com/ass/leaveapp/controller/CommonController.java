package com.ass.leaveapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ass.leaveapp.constant.AppConstant;
import com.ass.leaveapp.dto.ResponseDto;
import com.ass.leaveapp.entity.LeaveDetails;
import com.ass.leaveapp.service.CommonService;

@RestController
@RequestMapping(value=AppConstant.COMMON_CONTROLLER)
public class CommonController {
	
	@Autowired
	private CommonService commonService;
	
	@PostMapping(value=AppConstant.SAVE_EMPLOYEE_LEAVE_INFO)
	public @ResponseBody ResponseDto applyLeave(@RequestBody LeaveDetails leaveDetails) {
		System.out.println(this.getClass().getSimpleName());
		return commonService.applyLeave(leaveDetails);
	}

}
