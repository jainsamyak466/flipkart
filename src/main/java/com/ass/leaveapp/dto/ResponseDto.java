package com.ass.leaveapp.dto;

import java.io.Serializable;

public class ResponseDto implements Serializable {
	
	private String statuscode;
	
	private String status;
	
	private Object data;
	
	private String errormessage;

	public ResponseDto(String statuscode, String status, Object data, String errormessage) {
		super();
		this.statuscode = statuscode;
		this.status = status;
		this.data = data;
		this.errormessage = errormessage;
	}

	public String getStatuscode() {
		return statuscode;
	}

	public void setStatuscode(String statuscode) {
		this.statuscode = statuscode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getErrormessage() {
		return errormessage;
	}

	public void setErrormessage(String errormessage) {
		this.errormessage = errormessage;
	}
	
	

}
