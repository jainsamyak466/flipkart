package com.ass.leaveapp.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.ass.leaveapp.constant.AppConstant;

@Entity
@Table(name=AppConstant.LEAVE_DETAILS)
public class LeaveDetails implements Serializable {
	

	@Id
	@GenericGenerator(name="auto",strategy = "increment")
	@GeneratedValue(generator = "auto")
	@Column(name="id")
	private long id;
	
	@Column(name="employee_id")
	private long employeeid;
	
	@Column(name="start_date")
	private Date startdate;
	
	@Column(name="end_date")
	private Date enddate;
	
	@Column(name="total_days")
	private long totaldays;
	
	public LeaveDetails() {
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getEmployeeid() {
		return employeeid;
	}

	public void setEmployeeid(long employeeid) {
		this.employeeid = employeeid;
	}

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public Date getEnddate() {
		return enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	public long getTotaldays() {
		return totaldays;
	}

	public void setTotaldays(long totaldays) {
		this.totaldays = totaldays;
	}
	
	

}
