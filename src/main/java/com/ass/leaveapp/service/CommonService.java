package com.ass.leaveapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ass.leaveapp.dto.ResponseDto;
import com.ass.leaveapp.entity.LeaveDetails;
import com.ass.leaveapp.repository.LeaveDetailsRepository;

@Service
public class CommonService {
	
	@Autowired
	private LeaveDetailsRepository detailsRepository;
	
	public ResponseDto applyLeave(LeaveDetails leaveDetails) {
		try {
			LeaveDetails details = detailsRepository.save(leaveDetails);
			System.out.println("hi");
			return new ResponseDto("200", "success", details, null);
			
			
		} catch (Exception e) {
			return new ResponseDto("500", "failure", null, e.getLocalizedMessage());
		}
	}

}
